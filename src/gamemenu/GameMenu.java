package gamemenu;

/**
 * Interface for a generic Menu of the game.
 *
 */
public interface GameMenu {
    /**
     * Activates the Menu.
     * 
     * @param score : Score of the game 
     */
    void activate(int score);
}
