package piece;

/**
 * This class models the types witch the pieces could be. There are the 7 standard types and a custom one for all the custom pieces.
 *
 */
public enum Type {
    /**
     * Those are the type witch a piece could assume.
     */
    I, J, L, O, S, T, Z, CUSTOM
}
